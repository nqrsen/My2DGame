package Background;

import Entity.Player;
import Main.GamePanel;
import Main.KeyHandler;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class BackgroundManager {
    private GamePanel gamePanel;
    private Background[] background;

    private KeyHandler keyHandler;

    private Player player;

    private int currentLocation = 0;

    private long lastUpdateTime;
    private long waitTime = 1000 * 1000 * 1000;

    public BackgroundManager(GamePanel gamePanel, KeyHandler keyHandler, Player player) {
        lastUpdateTime = System.nanoTime();
        this.gamePanel = gamePanel;
        this.keyHandler = keyHandler;
        this.player = player;
        background = new Background[3];
        getTileImage();
    }

    public void getTileImage() {
        try {
            background[0] = new Background();
            background[0].setImage(ImageIO.read(new File("images/backgrounds/bedroom.png")));
            background[1] = new Background();
            background[1].setImage(ImageIO.read(new File("images/backgrounds/living.png")));
            background[2] = new Background();
            background[2].setImage(ImageIO.read(new File("images/backgrounds/kitchen.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void update() {
        if (keyHandler.ePressed && player.getX() == player.getLimits()[0] && (System.nanoTime() - lastUpdateTime) > waitTime) {
            if (currentLocation > 0) {
                currentLocation--;
                player.setX(player.getLimits()[1]);
            }
            lastUpdateTime = System.nanoTime();
        }
        if (keyHandler.ePressed && player.getX() == player.getLimits()[1] && (System.nanoTime() - lastUpdateTime) > waitTime) {
            if (currentLocation < 2) {
                currentLocation++;
                player.setX(player.getLimits()[0]);
            }
            lastUpdateTime = System.nanoTime();
        }
    }

    public void draw(Graphics2D g2) {
        if (background[currentLocation].getImage() != null) {
            g2.drawImage(background[currentLocation].getImage(), 0, 0, gamePanel.getScreenWidth(), gamePanel.getScreenHeight(), null);
        } else {
            System.out.println("Houston, we have a problem.");
        }
    }

    public int getCurrentLocation() {
        return currentLocation;
    }
}
