package Background;

import java.awt.image.BufferedImage;

public class Background {
    private BufferedImage image;
    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

}
