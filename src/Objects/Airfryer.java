package Objects;

import Entity.Player;
import Main.GamePanel;
import Main.KeyHandler;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Airfryer extends Object{
    private boolean gotSandwich = false;
    private BufferedImage text;
    public Airfryer(GamePanel gamePanel, KeyHandler keyHandler, Player player) {
        super(gamePanel,keyHandler,player);
        this.setName("airFryer");
        try {
            this.setImage( ImageIO.read(new File("images/objects/placeholder.png")));
            text =  ImageIO.read(new File("images/writings/e.png"));
            this.setWorldX(1080);
            this.setWorldY(340);
            this.setWidth(100);
            this.setHeight(100);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void update(){
        if ((player.getX() + player.getWidth() > getWorldX() &&
                player.getX() < getWorldX() + getWidth())) {
            if (keyHandler.ePressed) {
                gotSandwich = true;
            }
        }
    }

    public void draw(Graphics2D g2, GamePanel gamePanel){
        super.draw(g2, gamePanel);
        if ((player.getX() + player.getWidth() > getWorldX() &&
                player.getX() < getWorldX() + getWidth())
                && !gotSandwich) {
            g2.drawImage(text, 0, 0, gamePanel.getScreenWidth(), gamePanel.getScreenHeight(), null);
        }
    }
    public boolean isGotSandwich() {
        return gotSandwich;
    }
}
