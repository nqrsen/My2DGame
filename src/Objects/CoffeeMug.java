package Objects;

import Entity.Player;
import Main.GamePanel;
import Main.KeyHandler;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class CoffeeMug extends Object {
    private BufferedImage inventoryIcon;
    private boolean filled = false;
    private BufferedImage text;

    private BufferedImage inventoryText1;
    private BufferedImage inventoryText2;

    public CoffeeMug(GamePanel gamePanel, KeyHandler keyHandler, Player player) {
        super(gamePanel, keyHandler, player);
        this.setName("coffeeMug");
        try {
            this.setImage(ImageIO.read(new File("images/objects/coffeMug.png")));
            text = ImageIO.read(new File("images/writings/e.png"));
            inventoryIcon = ImageIO.read(new File("images/objects/inventoryCoffeeMug.png"));
            inventoryText1 = ImageIO.read(new File("images/writings/emptyCoffeeMug.png"));
            inventoryText2 =  ImageIO.read(new File("images/writings/CoffeedCoffeeMug.png"));
            this.setWorldX(540);
            this.setWorldY(400);
            this.setWidth(100);
            this.setHeight(100);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update() {
        if (    !isInInventory() &&
                (player.getX() + player.getWidth() > getWorldX() &&
                player.getX() < getWorldX() + getWidth())) {
            if (keyHandler.ePressed) {
                this.setImage(inventoryIcon);
                this.setWorldX(10 + 100 + 100);
                this.setWorldY(gamePanel.getScreenHeight() - 100);
                this.setWidth(100);
                this.setHeight(100);
                this.setInInventory(true);
                text = inventoryText1;
            }
        }

        if (filled){
            if (keyHandler.gPressed){
                filled = false;
                text = inventoryText1;
                gamePanel.playSoundEffect(4);
            }
        }
    }

    public void draw(Graphics2D g2, GamePanel gamePanel) {
        super.draw(g2, gamePanel);
        if ((player.getX() + player.getWidth() > getWorldX() &&
                player.getX() < getWorldX() + getWidth())
                && !inInventory) {
            g2.drawImage(text, 0, 0, gamePanel.getScreenWidth(), gamePanel.getScreenHeight(), null);
        }
        if (keyHandler.threePressed && isInInventory()){
            g2.drawImage(text,0,0,gamePanel.getScreenWidth(), gamePanel.getScreenHeight(), null);
        }
    }

    public void fillCup(){
        text = inventoryText2;
        filled = true;
    }

    public boolean isFilled() {
        return filled;
    }
}
