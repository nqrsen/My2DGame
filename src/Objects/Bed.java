package Objects;

import Entity.Player;
import Main.GamePanel;
import Main.KeyHandler;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class Bed extends Object {

    private BufferedImage text;
    private boolean inBed = false;
    private long lastUpdateTime;
    private long waitTime = 1000 * 1000 * 1000 / 2;
    public Bed(GamePanel gamePanel, KeyHandler keyHandler, Player player) {
        super(gamePanel,keyHandler,player);
        this.setName("bed");
        try {
            this.setImage( ImageIO.read(new File("images/objects/placeholder.png")));
            text = ImageIO.read(new File("images/writings/e.png"));
            this.setWorldX(310);
            this.setWorldY(500);
            this.setWidth(100);
            this.setHeight(100);
            lastUpdateTime = System.nanoTime();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void update(){
        if ((player.getX() + player.getWidth() > getWorldX() &&
                player.getX() < getWorldX() + getWidth())) {
            if (keyHandler.ePressed && System.nanoTime() - lastUpdateTime > waitTime) {
                inBed = !inBed;
                lastUpdateTime = System.nanoTime();
            }
        }
    }

    public void draw(Graphics2D g2,GamePanel gamePanel){
        if ((player.getX() + player.getWidth() > getWorldX() &&
                player.getX() < getWorldX() + getWidth())) {
            g2.drawImage(text, 0, 0, gamePanel.getScreenWidth(), gamePanel.getScreenHeight(), null);
        }
    }

    public boolean isInBed() {
        return inBed;
    }
}

