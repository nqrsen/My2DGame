package Objects;

import Entity.Player;
import Main.GamePanel;
import Main.KeyHandler;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Object {

    protected GamePanel gamePanel;

    protected KeyHandler keyHandler;

    protected Player player;
    private BufferedImage image;
    private String name;
    private int worldX, worldY;
    private int width, height;

    protected boolean inInventory = false;
    public Object(GamePanel gamePanel, KeyHandler keyHandler, Player player){
        this.gamePanel = gamePanel;
        this.keyHandler = keyHandler;
        this.player = player;
    }
    public BufferedImage getImage() {
        return image;
    }

    public void update(){};
    public void draw(Graphics2D g2, GamePanel gamePanel) {
        g2.drawImage(image, worldX, worldY, width, height, null);
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWorldX() {
        return worldX;
    }

    public void setWorldX(int worldX) {
        this.worldX = worldX;
    }

    public int getWorldY() {
        return worldY;
    }

    public void setGamePanel(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }

    public void setWorldY(int worldY) {
        this.worldY = worldY;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isInInventory() {
        return inInventory;
    }

    public void setInInventory(boolean inInventory) {
        if (inInventory){
            gamePanel.playSoundEffect(2);
        }
        this.inInventory = inInventory;
    }
}
