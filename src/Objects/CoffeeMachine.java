package Objects;

import Entity.Player;
import Main.GamePanel;
import Main.KeyHandler;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class CoffeeMachine extends Object{

    private boolean filledTheMug = false;

    private long lastFillTime;
    private BufferedImage text;
    public CoffeeMachine(GamePanel gamePanel, KeyHandler keyHandler, Player player) {
        super(gamePanel,keyHandler,player);
        this.setName("coffeeMachine");
        try {
            this.setImage( ImageIO.read(new File("images/objects/placeholder.png")));
            text =  ImageIO.read(new File("images/writings/e.png"));
            this.setWorldX(840);
            this.setWorldY(340);
            this.setWidth(100);
            this.setHeight(100);
            lastFillTime = System.nanoTime();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void update(){
        if ((player.getX() + player.getWidth() > getWorldX() &&
                player.getX() < getWorldX() + getWidth())) {
            if (!filledTheMug && keyHandler.ePressed) {
                filledTheMug = true;
                gamePanel.playSoundEffect(2);
                lastFillTime = System.nanoTime();
            }
        }
    }

    public void draw(Graphics2D g2, GamePanel gamePanel){
        super.draw(g2, gamePanel);
        if ((player.getX() + player.getWidth() > getWorldX() &&
                player.getX() < getWorldX() + getWidth())
                && !filledTheMug) {
            g2.drawImage(text, 0, 0, gamePanel.getScreenWidth(), gamePanel.getScreenHeight(), null);
        }
    }

    public boolean isFilledTheMug() {
        return filledTheMug;
    }

    public void setFilledTheMug(boolean filledTheMug) {
        this.filledTheMug = filledTheMug;
    }
}
