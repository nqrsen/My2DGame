package Objects;

import Entity.Player;
import Main.GamePanel;
import Main.KeyHandler;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Hoodie extends Object{
    private BufferedImage text;
    public Hoodie(GamePanel gamePanel, KeyHandler keyHandler, Player player) {
        super(gamePanel,keyHandler,player);
        this.setName("hoodie");
        try {
            this.setImage(ImageIO.read(new File("images/objects/hoodie.png")));
            text = ImageIO.read(new File("images/writings/hoodie.png"));
            this.setWorldX(10+100);
            this.setWorldY(gamePanel.getScreenHeight()-100);
            this.setWidth(100);
            this.setHeight(100);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void draw(Graphics2D g2, GamePanel gamePanel) {
        super.draw(g2,gamePanel);
        if (keyHandler.twoPressed){
            g2.drawImage(text,0,0,gamePanel.getScreenWidth(), gamePanel.getScreenHeight(), null);
        }
    }

}
