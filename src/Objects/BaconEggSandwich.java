package Objects;

import Entity.Player;
import Main.GamePanel;
import Main.KeyHandler;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class BaconEggSandwich extends Object{
    private BufferedImage text;
    private boolean eatenSandwich=false;
    public BaconEggSandwich(GamePanel gamePanel, KeyHandler keyHandler, Player player) {
        super(gamePanel,keyHandler,player);
        this.setName("baconEggSandwich");
        try {
            this.setImage(ImageIO.read(new File("images/objects/baconEggSandwich.png")));
            text = ImageIO.read(new File("images/writings/sandwich.png"));
            this.setWorldX(10+3*100);
            this.setWorldY(gamePanel.getScreenHeight()-100);
            this.setWidth(100);
            this.setHeight(100);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void update(){
        if (inInventory){
            if (keyHandler.fPressed){
                inInventory = false;
                gamePanel.playSoundEffect(3);
                eatenSandwich = true;
            }
        }
    }

    @Override
    public void draw(Graphics2D g2, GamePanel gamePanel){
        super.draw(g2, gamePanel);
        if (inInventory){
            g2.drawImage(this.getImage(), this.getWorldX(), this.getWorldY(), this.getWidth(), this.getHeight(), null);
        }
        if (keyHandler.fourPressed && isInInventory()){
            g2.drawImage(text,0,0,gamePanel.getScreenWidth(), gamePanel.getScreenHeight(), null);
        }
    };

    public boolean isEatenSandwich() {
        return eatenSandwich;
    }
}
