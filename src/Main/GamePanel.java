package Main;

import Background.BackgroundManager;
import Entity.Player;
import Entity.RNGFren;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GamePanel extends JPanel implements Runnable {

    public long getWinTime() {
        return winTime;
    }

    private final BufferedImage controlsText;
    private long winTime;
    private long snapTime;
    private final int FPS = 60;
    private final JFrame frame;

    private final JButton button;
    private final int originalTileSize = 16;
    private final int scale = 5;
    private final int tileSize = originalTileSize * scale; // 80 pixels
    private final int maxScreenCol = 20;
    private final int maxScreenRow = 10;
    private final int screenWidth = tileSize * maxScreenCol; // 1600 pixels
    private final int screenHeight = tileSize * maxScreenRow; // 800 pixels
    private Thread gameThread;
    private final Sound sound = new Sound();
    private final KeyHandler keyHandler = new KeyHandler();
    private final RNGFren rngFren = new RNGFren(this);
    private final Player player = new Player(this, keyHandler);
    private final BackgroundManager backgroundManager = new BackgroundManager(this, keyHandler, player);
    private final ObjectManager objectManager = new ObjectManager(this, backgroundManager, keyHandler, player);

    public GamePanel(JFrame frame,JButton button) throws IOException {
        this.frame = frame;
        this.button = button;
        controlsText = ImageIO.read(new File("images/writings/controls.png"));
        this.setPreferredSize(new Dimension(screenWidth, screenHeight));
        this.setBackground(Color.BLACK);
        this.setDoubleBuffered(true);
        this.addKeyListener(keyHandler);
        this.setFocusable(true);
        snapTime = System.nanoTime();
    }

    public void startGameThread() throws InterruptedException {
        gameThread = new Thread(this);
        setupRooms();
        playMusic(0);
        gameThread.start();
    }

    @Override
    public void run() {
        double drawInterval = 1000000000.0 / FPS;
        double delta = 0;
        long lastTime = System.nanoTime();
        long currentTime;
        long timer = 0;
        int drawCount = 0;

        while (gameThread != null) {

            currentTime = System.nanoTime();
            delta += (currentTime - lastTime) / drawInterval;
            timer += (currentTime - lastTime);
            lastTime = currentTime;

            if (delta >= 1) {
                try {
                    update();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                repaint();
                drawCount++;
                delta--;
            }

            if (timer >= 1000000000) {
                System.out.println(drawCount + "FPS");
                drawCount = 0;
                timer = 0;
            }

        }

    }

    public void update() throws InterruptedException {
        if (System.nanoTime() - snapTime > (long)1000*1000*1000*3 && keyHandler.qPressed){
            takeSnapshot(this);
            snapTime = System.nanoTime();
        }
        backgroundManager.update();
        objectManager.update();
        if (backgroundManager.getCurrentLocation() == 1) {
            if (rngFren != null) {
                rngFren.update();
            }
        }
        player.update();
        if (objectManager.isGameDone()){
            if (winTime == 0){
                playSoundEffect(1);
                DeathTimer John = new DeathTimer(getFrame());
                winTime = System.nanoTime();
            }
        }
        if (objectManager.isGameDone() && System.nanoTime() - winTime > (long)10*1000*1000*1000){
            ActionEvent event = new ActionEvent(button,ActionEvent.ACTION_PERFORMED,"",System.currentTimeMillis(),0);
            for (ActionListener listener : button.getActionListeners()){
                listener.actionPerformed(event);
            }
        }

    }

    void takeSnapshot(JPanel panel ){
        try {
            Robot robot = new Robot();
            int x1=0, x2=1920;
            int y1=0, y2=1080;
            Rectangle rect = new Rectangle(x1,y1,x2,y2);
            BufferedImage screenshot = robot.createScreenCapture(rect);
            //String path = "C:\\Users\\Palma\\OneDrive\\University\\java\\My2DGame\\screenshots";
            JFileChooser fileChooser = new JFileChooser();
            int r = fileChooser.showSaveDialog(null);
            if (r == JFileChooser.APPROVE_OPTION){
                ImageIO.write(screenshot,"png",fileChooser.getSelectedFile());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        backgroundManager.draw(g2);
        if (backgroundManager.getCurrentLocation() == 1) {
            try {
                if (rngFren != null) {
                    rngFren.draw(g2);
                }
            } catch (IOException | FontFormatException e) {
                throw new RuntimeException(e);
            }
        }
        player.draw(g2);
        objectManager.draw(g2);
        g2.drawImage(controlsText,0,0,getScreenWidth(),getScreenHeight(),null);
        g2.dispose();
    }

    public void setupRooms() {
        objectManager.setObject();
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public void playMusic(int i) {
        sound.setFile(i);
        sound.play();
        sound.loop();
    }

    public void stopMusic() {
        sound.stop();
    }

    public void playSoundEffect(int i) {
        sound.setFile(i);
        sound.play();
    }

    public JFrame getFrame() {
        return frame;
    }

    public int getFPS() {
        return FPS;
    }
}
