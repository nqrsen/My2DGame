package Main;

import javax.swing.*;
import java.awt.*;

public class JohnPanel extends JPanel {
    public JohnPanel() {
        setLayout(new BorderLayout(0,0));
        ImageIcon image = new ImageIcon("images/backgrounds/john.gif");
        JLabel displayField = new JLabel();
        displayField.setIcon(image);
        add(displayField);
    }

}
