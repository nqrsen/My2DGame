package Main;

import javax.swing.*;
import java.awt.event.WindowEvent;
import java.util.Timer;
import java.util.TimerTask;

public class DeathTimer {
    JFrame frame;
    private class DeathProgram extends TimerTask {
        public void run(){
            frame.dispatchEvent(new WindowEvent(frame,WindowEvent.WINDOW_CLOSING));
        }
    }

    DeathTimer(JFrame frame) {
        this.frame = frame;
        Timer timer = new Timer();
        TimerTask task = new DeathProgram();
        long delay = 12000 ;
        timer.schedule(task,delay);
    }

}
