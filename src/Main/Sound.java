package Main;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class Sound {
    private Clip clip;
    private final URL[] soundURL = new URL[30];

    public Sound() throws MalformedURLException {
        soundURL[0] = new File("sounds/bg.wav").toURI().toURL();
        soundURL[1] = new File("sounds/gameEnd.wav").toURI().toURL();
        soundURL[2] = new File("sounds/itemAcquired.wav").toURI().toURL();
        soundURL[3] = new File("sounds/nomnomnom.wav").toURI().toURL();
        soundURL[4] = new File("sounds/sip.wav").toURI().toURL();
    }

    public void setFile(int i) {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(soundURL[i]);
            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void play() {
        clip.start();
    }

    public void loop() {
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public void stop() {
        clip.stop();
    }

}
