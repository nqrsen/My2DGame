package Main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyHandler implements KeyListener {

    public boolean upPressed, downPressed, leftPressed, rightPressed;
    public boolean ePressed,fPressed,gPressed,qPressed;

    public boolean onePressed,twoPressed,threePressed,fourPressed;

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        if (code == KeyEvent.VK_Q){
            qPressed=true;
        }
        if (code == KeyEvent.VK_F){
            fPressed = true;
        }
        if (code == KeyEvent.VK_G){
            gPressed = true;
        }
        if (code == KeyEvent.VK_1){
            onePressed = true;
        }
        if (code == KeyEvent.VK_2) {
            twoPressed = true;
        }
        if (code == KeyEvent.VK_3){
            threePressed = true;
        }
        if (code == KeyEvent.VK_4){
            fourPressed = true;
        }
        if (code == KeyEvent.VK_W){
            upPressed = true;
        }
        if (code == KeyEvent.VK_S){
            downPressed = true;
        }
        if (code == KeyEvent.VK_A){
            leftPressed = true;
        }
        if (code == KeyEvent.VK_D){
            rightPressed = true;
        }
        if (code == KeyEvent.VK_E){
            ePressed = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int code = e.getKeyCode();
        if (code == KeyEvent.VK_Q){
            qPressed=false;
        }
        if (code == KeyEvent.VK_F){
            fPressed = false;
        }
        if (code == KeyEvent.VK_G){
            gPressed = false;
        }
        if (code == KeyEvent.VK_1){
            onePressed = false;
        }
        if (code == KeyEvent.VK_2) {
            twoPressed = false;
        }
        if (code == KeyEvent.VK_3){
            threePressed = false;
        }
        if (code == KeyEvent.VK_4){
            fourPressed = false;
        }
        if (code == KeyEvent.VK_W){
            upPressed = false;
        }
        if (code == KeyEvent.VK_S){
            downPressed = false;
        }
        if (code == KeyEvent.VK_A){
            leftPressed = false;
        }
        if (code == KeyEvent.VK_D){
            rightPressed = false;
        }
        if (code == KeyEvent.VK_E){
            ePressed = false;
        }
    }
}
