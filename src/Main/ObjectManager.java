package Main;

import Background.BackgroundManager;
import Entity.Player;
import Objects.*;
import Objects.Object;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ObjectManager {
    private boolean gameDone = false;
    private BufferedImage text;
    private final BufferedImage endText;
    private final BufferedImage notABugAFeature;
    private long notABugAFeatureTimer;

    GamePanel gamePanel;
    BackgroundManager backgroundManager;
    KeyHandler keyHandler;
    Player player;

    private final Object[] objects = new Object[7];
    private final int bed = 0;
    private final int laptop = 1;
    private final int hoodie = 2;
    private final int coffeMug = 3;
    private final int coffeMachine = 4;
    private final int airFryer = 5;
    private final int baconEggSandwich = 6;

    public ObjectManager(GamePanel gamePanel, BackgroundManager backgroundManager, KeyHandler keyHandler, Player player) throws IOException {
        this.gamePanel = gamePanel;
        this.backgroundManager = backgroundManager;
        this.keyHandler = keyHandler;
        this.player = player;
        endText = ImageIO.read(new File("images/writings/endtext.png"));
        text = ImageIO.read(new File("images/writings/objective.png"));
        notABugAFeature = ImageIO.read(new File("images/writings/notabugafeature.png"));
        setObject();
    }

    public void setObject() {
        objects[bed] = new Bed(gamePanel, keyHandler, player);
        objects[laptop] = new Laptop(gamePanel, keyHandler, player);
        objects[hoodie] = new Hoodie(gamePanel, keyHandler, player);
        objects[coffeMug] = new CoffeeMug(gamePanel, keyHandler, player);
        objects[coffeMachine] = new CoffeeMachine(gamePanel, keyHandler, player);
        objects[airFryer] = new Airfryer(gamePanel, keyHandler, player);
        objects[baconEggSandwich] = new BaconEggSandwich(gamePanel, keyHandler, player);
    }

    public void update() throws InterruptedException {

        objects[laptop].update();
        objects[hoodie].update();

        if (!gameDone &&
                objects[coffeMug].isInInventory() && ((CoffeeMug) objects[coffeMug]).isFilled() &&
                objects[baconEggSandwich].isInInventory() &&
                System.nanoTime() - notABugAFeatureTimer > 1500000000
        ) {
            gameDone = true;
        }

        if (!gameDone &&
                objects[coffeMug].isInInventory() && ((CoffeeMug) objects[coffeMug]).isFilled() &&
                ((BaconEggSandwich) objects[baconEggSandwich]).isEatenSandwich() &&
                !objects[baconEggSandwich].isInInventory()
        ) {
            objects[baconEggSandwich].setInInventory(true);
            text = notABugAFeature;
            notABugAFeatureTimer = System.nanoTime();
        }

        if (backgroundManager.getCurrentLocation() == 0) {
            objects[bed].update();
            player.goToSleep(((Bed) objects[bed]).isInBed());
        }
        if (backgroundManager.getCurrentLocation() == 1) {
            objects[coffeMug].update();
        }
        if (backgroundManager.getCurrentLocation() == 2) {
            if (objects[coffeMug].isInInventory()) {
                if (!((CoffeeMug) objects[coffeMug]).isFilled()) {
                    ((CoffeeMachine) objects[coffeMachine]).setFilledTheMug(false);
                }
                objects[coffeMachine].update();
            }
            objects[airFryer].update();
            if (!((BaconEggSandwich) objects[baconEggSandwich]).isEatenSandwich()
                    && !objects[baconEggSandwich].isInInventory()
                    && ((Airfryer) objects[airFryer]).isGotSandwich()) {
                objects[baconEggSandwich].setInInventory(true);
            }
            if (((CoffeeMachine) objects[coffeMachine]).isFilledTheMug()) {
                ((CoffeeMug) objects[coffeMug]).fillCup();
            }
        }
        if (objects[baconEggSandwich] != null && objects[baconEggSandwich].isInInventory()) {
            objects[baconEggSandwich].update();
        }
        if (objects[coffeMug] != null && objects[coffeMug].isInInventory()) {
            objects[coffeMug].update();
        }

    }

    public void draw(Graphics2D g2) {
        if (objects[laptop] != null) {
            objects[laptop].draw(g2, gamePanel);
        }

        if (objects[hoodie] != null) {
            objects[hoodie].draw(g2, gamePanel);
        }

        if (backgroundManager.getCurrentLocation() == 0) {
            if (objects[bed] != null) {
                objects[bed].draw(g2, gamePanel);
            }
        }
        if (backgroundManager.getCurrentLocation() == 1 || objects[coffeMug] != null && objects[coffeMug].isInInventory()) {
            if (objects[coffeMug] != null) {
                objects[coffeMug].draw(g2, gamePanel);
            }
        }
        if (backgroundManager.getCurrentLocation() == 2) {
            if (objects[airFryer] != null) {
                objects[airFryer].draw(g2, gamePanel);
            }
            if (objects[coffeMug] != null) {
                if (objects[coffeMug].isInInventory() && objects[coffeMachine] != null) {
                    objects[coffeMachine].draw(g2, gamePanel);
                }
            }
        }
        if (objects[baconEggSandwich] != null && objects[baconEggSandwich].isInInventory()) {
            if (objects[baconEggSandwich] != null) {
                objects[baconEggSandwich].draw(g2, gamePanel);
            }
        }

        if (gameDone) {
            text = endText;
        }

        if (gameDone && System.nanoTime() - gamePanel.getWinTime() < 2 * 1000000000) {
            g2.drawImage(text, 0, 0, gamePanel.getScreenWidth(), gamePanel.getScreenHeight(), null);
        }
        if (!gameDone) {
            g2.drawImage(text, 0, 0, gamePanel.getScreenWidth(), gamePanel.getScreenHeight(), null);
        }

    }
    public boolean isGameDone() {
        return gameDone;
    }

}
