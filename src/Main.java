import javax.swing.*;
import Main.GamePanel;
import Main.JohnPanel;

import java.awt.*;
import java.io.IOException;

public class Main {
    static CardLayout cardLayout;
    static JPanel cardPanel;

    public static void main(String[] args) throws IOException, InterruptedException {
        cardLayout = new CardLayout();
        cardPanel = new JPanel();
        cardPanel.setLayout(cardLayout);

        JFrame window = new JFrame();
        window.setSize(1600,800);
        window.setResizable(false);

        JButton switchPanel = new JButton();

        GamePanel gamePanel = new GamePanel(window,switchPanel);
        gamePanel.startGameThread();
        JohnPanel otherPanel = new JohnPanel();

        cardPanel.add(gamePanel,"game");
        cardPanel.add(otherPanel,"perish");

        cardLayout.show(cardPanel,"game");

        switchPanel.addActionListener(e -> cardLayout.show(cardPanel,"perish"));

        window.add(cardPanel);

        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setTitle("2D Adventure");
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(true);

    }
}