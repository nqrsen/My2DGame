package Entity;

import Main.GamePanel;
import Main.KeyHandler;

import java.awt.image.BufferedImage;

// superclass for player and other character classes
public class Entity {
    protected int x,y;
    protected int speed;

    protected BufferedImage left1, left2, right1, right2, sleeping1, sleeping2;
    protected String direction;
    protected int spriteCounter = 0;
    protected int spriteNumber = 1;

    GamePanel gamePanel;
    KeyHandler keyHandler;
    protected int width = 300;
    protected int height = 300;

    protected int[] limits;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getWidth() {
        return width;
    }
}
