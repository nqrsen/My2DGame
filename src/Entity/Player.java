package Entity;

import javax.imageio.ImageIO;

import Main.GamePanel;
import Main.KeyHandler;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Player extends Entity {

    private BufferedImage textUp;
    private BufferedImage textDown;

    private boolean isSleeping = false;

    public Player(GamePanel gamePanel, KeyHandler keyHandler) {
        this.gamePanel = gamePanel;
        this.keyHandler = keyHandler;
        limits = new int[]{10, gamePanel.getScreenWidth() - 10 - width};
        setDefaultValues();
        getPlayerImage();

    }

    public void setDefaultValues() {
        x = limits[0]+10;
        y = 480;
        speed = 4;
        direction = "right";
    }

    public void getPlayerImage() {
        try {
            left1 = ImageIO.read(new File("images/character/left1.png"));
            left2 = ImageIO.read(new File("images/character/left2.png"));
            right1 = ImageIO.read(new File("images/character/right1.png"));
            right2 = ImageIO.read(new File("images/character/right2.png"));
            sleeping1 = ImageIO.read(new File("images/character/sleeping1.png"));
            sleeping2 = ImageIO.read(new File("images/character/sleeping2.png"));
            textUp = ImageIO.read(new File("images/writings/up.png"));
            textDown = ImageIO.read(new File("images/writings/down.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void update() {
        if (isSleeping) {
            y = 355;
            speed = 0;
        }else {
            y = 480;
            speed = 4;
        }
        if (keyHandler.leftPressed || keyHandler.rightPressed) {
            if (keyHandler.leftPressed) {
                if (x - speed > limits[0]) {
                    x = x - speed;
                } else {
                    x = limits[0];
                }

                direction = "left";
            }
            if (keyHandler.rightPressed) {
                if (x + speed < limits[1]) {
                    x = x + speed;
                } else {
                    x = limits[1];
                }
                direction = "right";
            }
            spriteCounter++;
            if (spriteCounter > gamePanel.getFPS() / 4) {
                if (spriteNumber == 1) {
                    spriteNumber = 2;
                } else if (spriteNumber == 2) {
                    spriteNumber = 1;
                }
                spriteCounter = 0;
            }
        }

    }

    public void draw(Graphics2D g2) {
        if (keyHandler.upPressed) {
            g2.drawImage(textUp,0,0,gamePanel.getScreenWidth(), gamePanel.getScreenHeight(), null);
        }
        if (keyHandler.downPressed) {
            g2.drawImage(textDown,0,0,gamePanel.getScreenWidth(), gamePanel.getScreenHeight(), null);
        }

        BufferedImage image = null;
        if (isSleeping){
            if (spriteNumber == 1) {
                image = sleeping1;
            }
            if (spriteNumber == 2) {
                image = sleeping2;
            }
        }else {
            switch (direction) {
                case "left" -> {
                    if (spriteNumber == 1) {
                        image = left1;
                    }
                    if (spriteNumber == 2) {
                        image = left2;
                    }
                }
                case "right" -> {
                    if (spriteNumber == 1) {
                        image = right1;
                    }
                    if (spriteNumber == 2) {
                        image = right2;
                    }
                }
            }
        }
        g2.drawImage(image, x, y, width, height, null);

    }

    public void goToSleep(boolean value){
        isSleeping = value;
    }
    public int[] getLimits() {
        return limits;
    }
}
