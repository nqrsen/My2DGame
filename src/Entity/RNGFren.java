package Entity;

import Main.GamePanel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class RNGFren extends Entity {
    private boolean spawned;

    public RNGFren(GamePanel gamePanel) throws IOException {
        this.gamePanel = gamePanel;
        Random rand = new Random();
        int chance = rand.nextInt() % 100;
        if (chance > 25) {
            spawned = true;
        }
        if (spawned) {
            setDefaultValues();
            getFrenImage();
        }
    }

    public void setDefaultValues() {
        x = 130;
        y = 290;
        speed = 0;
        direction = "left";
    }

    public void getFrenImage() throws IOException {
        left1 = ImageIO.read(new File("images/character/fren1.png"));
        left2 = ImageIO.read(new File("images/character/fren2.png"));
    }

    public void update() {
        spriteCounter++;
        if (spriteCounter > gamePanel.getFPS() / 2) {
            if (spriteNumber == 1) {
                spriteNumber = 2;
            } else if (spriteNumber == 2) {
                spriteNumber = 1;
            }
            spriteCounter = 0;
        }
    }

    public void draw(Graphics2D g2) throws IOException, FontFormatException {
        //GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Font minecraft = Font.createFont(Font.TRUETYPE_FONT,new File("images/font/font.ttf")).deriveFont(38f);
        BufferedImage image = null;
        if (spriteNumber == 1) {
            image = left1;
        }
        if (spriteNumber == 2) {
            image = left2;
        }
        g2.drawImage(image, x, y, width, height, null);
        g2.setFont(minecraft);
        g2.setColor(new Color(252,228,255));
        g2.drawString("<Wife>",x+100,y+50);
    }

    public boolean isSpawned() {
        return spawned;
    }
}

